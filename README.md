Static websites for previous GUADEC conferences.

The content of this repository moved to https://gitlab.gnome.org/Infrastructure/guadec-web. Each website has its own static-$YEAR branch, which is deployed to GNOME Openshift Platform.